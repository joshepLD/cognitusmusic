package com.example.cognitus.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Registro (
    @SerializedName("usr_rutafoto")@Expose val RegistroFoto: String?,
    @SerializedName("usr_email")@Expose val RegistroCorreo: String?,
    @SerializedName("usr_id")@Expose val RegistroId: String?,
    @SerializedName("usr_apm")@Expose val RegistroApm: String?,
    @SerializedName("usr_nombre")@Expose val RegistroNombre: String?,
    @SerializedName("usr_app")@Expose val RegistroApp: String?
)