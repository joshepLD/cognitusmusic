package com.example.cognitus.model.response

import com.example.cognitus.model.model.Usuario
import com.google.gson.annotations.SerializedName

class UsuarioResponse (@SerializedName("valido") var valido:String,
                       @SerializedName("usuario") var usuario: Usuario
)