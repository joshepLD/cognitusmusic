package com.example.cognitus.model.response

import com.example.cognitus.model.model.Orden
import com.google.gson.annotations.SerializedName

class OrdenResponse (@SerializedName("valido") var OValido:String,
                     @SerializedName("respuesta") var ORespuesta: Orden
)