package com.example.cognitus.model.response

import com.google.gson.annotations.SerializedName

class RecuperarResponse (@SerializedName("valido") var valido:String,
                         @SerializedName("mesaje") var mensaje: String
)