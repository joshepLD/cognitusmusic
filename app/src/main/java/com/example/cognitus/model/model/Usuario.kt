package com.example.cognitus.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Usuario (
        @SerializedName("usr_id")@Expose val UsrId: String?,
        @SerializedName("usr_nombre")@Expose val UsrNombre: String?,
        @SerializedName("usr_rutafoto")@Expose val UsrFoto: String?,
        @SerializedName("usr_email")@Expose val Usrcorreo: String?,
        @SerializedName("usr_apm")@Expose val Usrapm: String?,
        @SerializedName("usr_app")@Expose val Usrapp: String?
    )