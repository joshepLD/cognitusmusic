package com.example.cognitus.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Actualizar (
    @SerializedName("usr_rutafoto")@Expose val AFoto: String?,
    @SerializedName("usr_email")@Expose val ACorreo: String?,
    @SerializedName("usr_id")@Expose val AId: String?,
    @SerializedName("usr_apm")@Expose val AApm: String?,
    @SerializedName("usr_nombre")@Expose val ANombre: String?,
    @SerializedName("usr_app")@Expose val AApp: String?
)