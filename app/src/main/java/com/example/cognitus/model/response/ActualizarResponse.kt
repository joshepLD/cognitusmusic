package com.example.cognitus.model.response

import com.example.cognitus.model.model.Actualizar
import com.google.gson.annotations.SerializedName

class ActualizarResponse (@SerializedName("valido") var Avalido:String,
                          @SerializedName("mensaje") var Amensaje: String,
                          @SerializedName("registro") var Aregistro: Actualizar
)