package com.example.cognitus.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Productos (
    @SerializedName("prod_desc")@Expose val PDesc: String?,
    @SerializedName("prod_titulo")@Expose val PTitulo: String?,
    @SerializedName("prod_precio")@Expose val PPrecio: String?,
    @SerializedName("prod_img")@Expose val PImagen: String?,
    @SerializedName("prod_id")@Expose val PId: String?
)
