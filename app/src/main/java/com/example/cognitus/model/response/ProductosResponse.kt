package com.example.cognitus.model.response

import com.example.cognitus.model.model.Productos
import com.google.gson.annotations.SerializedName

class ProductosResponse (@SerializedName("productos") var Productos: List<Productos>)
