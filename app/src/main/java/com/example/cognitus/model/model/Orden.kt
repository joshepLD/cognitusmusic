package com.example.cognitus.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Orden (
        @SerializedName("orden")@Expose val Orden: String?
    )