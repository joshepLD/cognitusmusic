package com.example.cognitus.model.response

import com.example.cognitus.model.model.Registro
import com.example.cognitus.model.model.Usuario
import com.google.gson.annotations.SerializedName

class RegistroResponse (@SerializedName("valido") var valido:String,
                        @SerializedName("mensaje") var mensaje:String,
                        @SerializedName("registro") var registro: Registro
)