package com.example.cognitus.dao

import androidx.room.*
import com.example.cognitus.entity.TaskEntity
import java.util.*

@Dao
interface TaskDao {
    @Query("SELECT * FROM tareas_entity")
    fun getAllTasks(): MutableList<TaskEntity>

    @Insert
    fun addTask(taskEntity : TaskEntity):Long

    @Query("SELECT * FROM tareas_entity where id like :id ORDER BY time(fecha) asc")
    fun getTaskById(id: Long): TaskEntity

    @Delete
    fun deleteTask(taskEntity: TaskEntity):Int

    @Update
    fun updateTask(taskEntity: TaskEntity):Int


}