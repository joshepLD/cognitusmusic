package com.example.cognitus.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.cognitus.R
import kotlinx.android.synthetic.main.activity_home.*

class Home : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        lnTareasHome.setOnClickListener{
            val intent = Intent(this, Tareas::class.java)
            startActivity(intent)
        }
        lnPerfilHome.setOnClickListener{
            val intent = Intent(this, Perfil::class.java)
            startActivity(intent)
        }

        btnCerrarS.setOnClickListener{
            val sharedPreference =  getSharedPreferences("mi_app_music", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putString("usr_id","")
            editor.putString("usr_id", "")
            editor.putString("usr_nombre", "")
            editor.putString("usr_foto", "")
            editor.putString("usr_correo", "")
            editor.putString("usr_app", "")
            editor.putString("usr_apm", "")

            editor.commit()

            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        lnProductosHome.setOnClickListener{
            val intent = Intent(this, ListProductos::class.java)
            startActivity(intent)
        }

        lnFirmaHome.setOnClickListener{
            val intent = Intent(this, Firma::class.java)
            startActivity(intent)
        }
    }
}
