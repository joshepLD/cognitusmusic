package com.example.cognitus.activities

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.CalendarView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.cognitus.R
import com.example.cognitus.model.response.RegistroResponse
import com.example.cognitus.model.response.UsuarioResponse
import com.example.cognitus.task.APIService
import com.example.cognitus.utilities.Utils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_perfil.*
import kotlinx.android.synthetic.main.activity_registro.*
import kotlinx.android.synthetic.main.activity_registro.edtContraseña
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class Registro : AppCompatActivity() {
    private val GALLERY = 1
    private val CAMERA = 2
    private var mediaPath: String? = null
    private var postPath: String? = null
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        TvRegistroIn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        CvEditarRegistar.setOnClickListener {
            showPictureDialog()
        }

        btnIniciarR.setOnClickListener{
            if(edtNombre.text.isEmpty() || edtApellidoP.text.isEmpty() || edtApellidoM.text.isEmpty() || edtContraseña.text.isEmpty()  || edtCorreoR.text.isEmpty()){
                Toast.makeText(this, "Los campos estan vacios", Toast.LENGTH_LONG).show()
            }else{
                if (Utils.isEmailValid("" + edtCorreoR.text)){
                    if (Utils.veryfyAvailableNetwork(this)) {
                        if (postPath != null || postPath == ""){
                            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                            searchByName(
                                edtCorreoR.text.toString().toLowerCase(),
                                edtNombre.text.toString().toLowerCase(),
                                edtApellidoP.text.toString().toLowerCase(),
                                edtApellidoM.text.toString().toLowerCase(),
                                edtContraseña.text.toString().toLowerCase()
                            )
                        }else{
                            Toast.makeText(
                                applicationContext,
                                "Agregue una imagen!",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }else{
                        Toast.makeText(
                            applicationContext,
                            "No cuenta con conexion a internet!",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }else{
                    Toast.makeText(this, "Formato de correo no valido", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun searchByName(query: String, query2: String, query3: String, query4: String, query5: String) {
        Log.e("Tag", "Hola1")
        doAsync {
            Log.e("Tag", "Hola2")
            val img = File(postPath)
            val partes = ArrayList<MultipartBody.Part>()
            partes.add(MultipartBody.Part.createFormData("ApiCall", "setRegisterUsr"))
            partes.add(MultipartBody.Part.createFormData("email", query))
            partes.add(MultipartBody.Part.createFormData("nombre", query2))
            partes.add(MultipartBody.Part.createFormData("app", query3))
            partes.add(MultipartBody.Part.createFormData("apm", query4))
            partes.add(MultipartBody.Part.createFormData("nip", query5))
            partes.add(MultipartBody.Part.createFormData(
                "archivo",
                img?.name,
                RequestBody.create(MediaType.parse("image/*"), img)))
            Log.e("Tag", "Hola2.5")
            val getResponse =
            getRetrofit().create(APIService::class.java).getCharacterByRegistrer(partes)?.execute()

            if (getResponse != null) {
                Log.e("Tag", "Hola3")
                val call = getResponse?.body() as RegistroResponse
                Log.i("Resp", "${call.valido} ")
                Log.i("Resp", "${call.registro.RegistroFoto} ")

                val sharedPreference =  getSharedPreferences("mi_app_music",Context.MODE_PRIVATE)
                var editor = sharedPreference.edit()
                editor.putString("usr_id",call.registro.RegistroId)
                editor.putString("usr_nombre",call.registro.RegistroNombre)
                editor.putString("usr_foto",call.registro.RegistroFoto)
                editor.putString("usr_correo",call.registro.RegistroCorreo)
                editor.putString("usr_app",call.registro.RegistroApp)
                editor.putString("usr_apm",call.registro.RegistroApm)
                editor.commit()

                val intent = Intent(applicationContext, Home::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }
    
    /////////

    private fun showPictureDialog(){
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Foto del perfil")
        val pictureDialogItems = arrayOf("Galería", "Cámara")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallery()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallery(){
        val galleryIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera(){
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data!!.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                try {
                    val cursor = contentResolver.query(contentURI!!, filePathColumn, null, null, null)
                    assert(cursor != null)
                    cursor!!.moveToFirst()
                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    mediaPath = cursor.getString(columnIndex)
                    // Set the Image in ImageView for Previewing the Media
                    CvRegistrar.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
                    cursor.close()
                    postPath = mediaPath
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@Registro, "Hubo un error!", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            CvRegistrar!!.setImageBitmap(thumbnail)
            postPath = saveImage(thumbnail)

            Log.e("Return", "Retotno :"+postPath)
            Toast.makeText(this@Registro, "Imagen Guardada", Toast.LENGTH_SHORT).show()
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY
        )
        //build the directory structure
        Log.d("file", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }
    companion object {
        private val IMAGE_DIRECTORY = "/demonuts"
    }
}
