package com.example.cognitus.activities

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.cognitus.R
import com.example.cognitus.model.response.ActualizarResponse
import com.example.cognitus.model.response.RegistroResponse
import com.example.cognitus.task.APIService
import com.example.cognitus.utilities.DialogoAlerta
import com.example.cognitus.utilities.Utils
import kotlinx.android.synthetic.main.activity_perfil.*
import kotlinx.android.synthetic.main.activity_registro.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class Perfil : AppCompatActivity() {

    private val GALLERY = 1
    private val CAMERA = 2

    private var IdUsuario: String? = null

    private var mediaPath: String? = null
    private var postPath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)

        val  sharedPreference =  getSharedPreferences(
            "mi_app_music",
            Context.MODE_PRIVATE
        )
        IdUsuario = sharedPreference.getString("usr_id","")
        val usrNombre = sharedPreference.getString("usr_nombre","")
        val usrCorreo = sharedPreference.getString("usr_correo","")
        val usrApp = sharedPreference.getString("usr_app","")
        val usrApm = sharedPreference.getString("usr_apm","")
        postPath = sharedPreference.getString("usr_foto","")

        Log.e("Id___",IdUsuario)


        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load("http://35.155.161.8:8080/WSExample/${postPath}")
        requestBuilder.into(CvPerfil)

        edtNombrePerfil.setText(usrNombre)
        edtApellidoPPerfil.setText(usrApp)
        edtApellidoMPerfil.setText(usrApm)
        edtCorreoPerfil.setText(usrCorreo)

        CvEditar.setOnClickListener {
            showPictureDialog()
        }

        btnActualizarPerfil.setOnClickListener{
            if(edtNombrePerfil.text.isEmpty() || edtApellidoPPerfil.text.isEmpty() || edtApellidoMPerfil.text.isEmpty()|| edtCorreoPerfil.text.isEmpty() || edtContraseñaPerfil.text.isEmpty()){
                Toast.makeText(this, "Los campos estan vacios", Toast.LENGTH_LONG).show()
            }else{
                if (Utils.isEmailValid("" + edtCorreoPerfil.text)){
                    if (Utils.veryfyAvailableNetwork(this)) {

                        Log.e("nombre", usrNombre)
                        Log.e("correo", usrCorreo)

                        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                        searchByName(
                            IdUsuario.toString().toLowerCase(),
                            edtNombrePerfil.text.toString().toLowerCase(),
                            edtApellidoPPerfil.text.toString().toLowerCase(),
                            edtApellidoMPerfil.text.toString().toLowerCase(),
                            edtCorreoPerfil.text.toString().toLowerCase(),
                            edtContraseñaPerfil.text.toString().toLowerCase()
                        )
                    }else{
                        Toast.makeText(
                            applicationContext,
                            "No cuenta con conexion a internet!",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }else{
                    Toast.makeText(this, "Formato de correo no valido", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun searchByName(id: String, nombre: String, app: String, apm: String, correo: String, pass: String) {
        Log.e("Tag", "Hola1")
        doAsync {
            Log.e("Tag", "Hola2")
            val img = File(postPath)
            val partes = ArrayList<MultipartBody.Part>()
            partes.add(MultipartBody.Part.createFormData(
                "archivo",
                img?.name,
                RequestBody.create(MediaType.parse("image/*"), img)))
            partes.add(MultipartBody.Part.createFormData("nombre", nombre))
            partes.add(MultipartBody.Part.createFormData("apm", apm))
            partes.add(MultipartBody.Part.createFormData("app", app))
            partes.add(MultipartBody.Part.createFormData("email", correo))
            partes.add(MultipartBody.Part.createFormData("nip", pass))
            partes.add(MultipartBody.Part.createFormData("ApiCall", "editRegisterUsr"))
            partes.add(MultipartBody.Part.createFormData("usrid", id))
            Log.e("Tag", "Hola2.5")
            val getResponse =
                getRetrofit().create(APIService::class.java).getCharacterByActualizar(partes)?.execute()

                Log.e("Tag", "Hola3")
                val call = getResponse?.body() as ActualizarResponse
                Log.i("Resp", "${call.Avalido} ")
                Log.i("Resp", "${call.Aregistro.AFoto} ")
                uiThread {
                    if(call.Avalido == "1") {
                        val sharedPreference =  getSharedPreferences("mi_app_music", Context.MODE_PRIVATE)
                        var editor = sharedPreference.edit()
                        editor.putString("usr_id", call.Aregistro.AId)
                        editor.putString("usr_nombre", call.Aregistro.ANombre)
                        editor.putString("usr_foto", call.Aregistro.AFoto)
                        editor.putString("usr_correo", call.Aregistro.ACorreo)
                        editor.putString("usr_app", call.Aregistro.AApp)
                        editor.putString("usr_apm", call.Aregistro.AApm)
                        editor.commit()
                        Toast.makeText(this@Perfil, "Perfil Actualizado!", Toast.LENGTH_SHORT).show()
                    }else{

                        showErrorDialog()
                    }
                }

        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }

    ///////

    private fun showPictureDialog(){
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Foto del perfil")
        val pictureDialogItems = arrayOf("Galería", "Cámara")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallery()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallery(){
        val galleryIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera(){
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data!!.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                try {
                    val cursor = contentResolver.query(contentURI!!, filePathColumn, null, null, null)
                    assert(cursor != null)
                    cursor!!.moveToFirst()
                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    mediaPath = cursor.getString(columnIndex)
                    // Set the Image in ImageView for Previewing the Media
                    CvPerfil.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
                    cursor.close()
                    postPath = mediaPath
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@Perfil, "Hubo un error!", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            CvPerfil!!.setImageBitmap(thumbnail)
            postPath = saveImage(thumbnail)
            Toast.makeText(this@Perfil, "Imagen Guardada", Toast.LENGTH_SHORT).show()
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY
        )
        //build the directory structure
        Log.d("file", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }
    companion object {
        private val IMAGE_DIRECTORY = "/demonuts"
    }
}
