package com.example.cognitus.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.cognitus.R
import com.example.cognitus.model.response.OrdenResponse
import com.example.cognitus.model.response.UsuarioResponse
import com.example.cognitus.task.APIService
import com.example.cognitus.utilities.DialogoAlerta
import com.example.cognitus.utilities.Utils
import kotlinx.android.synthetic.main.activity_detail_product.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DetailProduct : AppCompatActivity() {
    lateinit var strId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Contacto"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)

        }

        var iVNew = findViewById<ImageView>(R.id.iVProd)
        var tVTitle = findViewById<TextView>(R.id.tVTitleProd)
        var tVPrecio = findViewById<TextView>(R.id.tVPrecioProd)
        var tVContent = findViewById<TextView>(R.id.tVContentProd)
        val urlImg: String = intent.getStringExtra("img_url")
        val title: String = intent.getStringExtra("title")
        val conten: String = intent.getStringExtra("desc")
        val precio: String = intent.getStringExtra("precio")
        strId = intent.getStringExtra("prodID")
        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load("http://35.155.161.8:8080/WSExample/${urlImg}")
        requestBuilder.into(iVNew)
        tVTitle.setText(title)
        tVPrecio.setText(precio)
        tVContent.setText(conten)

        btnCarrito.setOnClickListener{
            if (Utils.veryfyAvailableNetwork(this)) {
                searchByName(strId)
            } else {
                Toast.makeText(
                    applicationContext,
                    "No cuenta con conexion a internet!",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

    }

    private fun searchByName(id: String) {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).getOrden("$id").execute()
            val response = call.body() as OrdenResponse
            uiThread {
                if(response.OValido == "1") {
                    DialogoAlerta.crearDialogo(this@DetailProduct,"Gracias por su compra", "su numero de compra es: "+ response.ORespuesta.Orden)
                }else{
                    Log.e("Error",response.OValido)
                    showErrorDialog()
                }
            }
        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }

}
