package com.example.cognitus.activities

import android.app.Application
import androidx.room.Room
import com.example.cognitus.database.TasksDatabase

class MisNotasApp: Application() {

    companion object {
        lateinit var database: TasksDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database =  Room.databaseBuilder(this, TasksDatabase::class.java, "tareas-db").build()
    }
}