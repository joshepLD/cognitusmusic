package com.example.cognitus.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cognitus.R
import com.example.cognitus.adapter.ProductoAdapter
import com.example.cognitus.model.response.ProductosResponse
import com.example.cognitus.model.response.UsuarioResponse
import com.example.cognitus.task.APIService
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ListProductos : AppCompatActivity() {
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_productos)
        searchByName()
    }
    private fun searchByName() {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).getCharacterByProductos().execute()
            val response = call.body() as ProductosResponse
            uiThread {
                linearLayoutManager = LinearLayoutManager(applicationContext)

                val rvProductos = findViewById<RecyclerView>(R.id.rvProductos)
                rvProductos.layoutManager = linearLayoutManager
                rvProductos.adapter = ProductoAdapter(this@ListProductos, response.Productos)
            }
        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun initCharacter(response: UsuarioResponse) {
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }
}
