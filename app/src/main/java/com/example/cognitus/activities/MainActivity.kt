package com.example.cognitus.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cognitus.R
import com.example.cognitus.model.response.UsuarioResponse
import com.example.cognitus.task.APIService
import com.example.cognitus.utilities.Utils
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        TvRecuperarPass.setOnClickListener{
            val intent = Intent(this, RecuperarPass::class.java)
            startActivity(intent)
        }
        TvRegistro.setOnClickListener{
            val intent = Intent(this, Registro::class.java)
            startActivity(intent)
        }
        btnIniciar.setOnClickListener{
            if(edtUsuario.text.isEmpty() || edtContraseña.text.isEmpty()){
                Toast.makeText(this, "Los campos estan vacios", Toast.LENGTH_LONG).show()
            }else{
                if (Utils.isEmailValid("" + edtUsuario.text)){
                    if (Utils.veryfyAvailableNetwork(this)) {
                        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                        searchByName(edtUsuario.text.toString().toLowerCase(), edtContraseña.text.toString().toLowerCase())
                    }else{
                        Toast.makeText(
                            applicationContext,
                            "No cuenta con conexion a internet!",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }else{
                    Toast.makeText(this, "Formato de correo no valido", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    private fun searchByName(query: String, query2:String) {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).getCharacterByName("$query", "$query2").execute()
            val response = call.body() as UsuarioResponse
            uiThread {
                if(response.valido == "1") {
                    initCharacter(response)
                }else{
                    Log.e("Error",response.valido)
                    showErrorDialog()
                }
            }
        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun initCharacter(response: UsuarioResponse) {
        if(response.valido == "1"){
            val sharedPreference =  getSharedPreferences("mi_app_music",Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putString("usr_id", response.usuario.UsrId)
            editor.putString("usr_nombre",response.usuario.UsrNombre)
            editor.putString("usr_foto",response.usuario.UsrFoto)
            editor.putString("usr_correo",response.usuario.Usrcorreo)
            editor.putString("usr_app",response.usuario.Usrapp)
            editor.putString("usr_apm",response.usuario.Usrapm)
            editor.commit()

            val intent = Intent(applicationContext, Home::class.java)
            intent.putExtra("usr_nombre",  response.usuario.UsrNombre)
            intent.putExtra("usr_id", response.usuario.UsrId)
            startActivity(intent)
            finish()
        }

    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }
}
