package com.example.cognitus.activities

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.cognitus.R
import com.example.cognitus.model.response.RecuperarResponse
import com.example.cognitus.task.APIService
import com.example.cognitus.utilities.Utils
import kotlinx.android.synthetic.main.activity_recuperar_pass.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RecuperarPass : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recuperar_pass)

        btnIniciarPass.setOnClickListener {
            if(edtCorreoPass.text.isEmpty() || edtUsuarioPass.text.isEmpty()){
                Toast.makeText(this, "Los campos estan vacios", Toast.LENGTH_LONG).show()
            }else{
                if (Utils.isEmailValid("" + edtCorreoPass.text)){
                    if (Utils.veryfyAvailableNetwork(this)) {
                        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                        searchByName(edtCorreoPass.text.toString().toLowerCase(), edtUsuarioPass.text.toString().toLowerCase())
                    }else{
                        Toast.makeText(
                            applicationContext,
                            "No cuenta con conexion a internet!",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }else{
                    Toast.makeText(this, "Formato de correo no valido", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun searchByName(query: String, query2:String) {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).getCharacterByRecuperar("$query", "$query2").execute()
            val response = call.body() as RecuperarResponse
            uiThread {
                if(response.valido == "1") {
                    val dialogBuilder = AlertDialog.Builder(this@RecuperarPass)
                    // set message of alert dialog
                    dialogBuilder.setMessage(response.mensaje)
                        // if the dialog is cancelable
                        .setCancelable(false)
                        // positive button text and action
                        .setPositiveButton("Aceptar", DialogInterface.OnClickListener {
                                dialog, id -> finish()
                            val intent = Intent(applicationContext, MainActivity::class.java)
                            startActivity(intent)
                        })
                    // create dialog box
                    val alert = dialogBuilder.create()
                    // set title for alert dialog box
                    alert.setTitle("Contraseña Recuperada")
                    // show alert dialog
                    alert.show()
                }else{
                    Toast.makeText(this@RecuperarPass, response.mensaje + " intente de nuevo", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }
}
