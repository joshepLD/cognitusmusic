package com.example.cognitus.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.cognitus.R
import com.github.gcacace.signaturepad.views.SignaturePad
import kotlinx.android.synthetic.main.activity_firma.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

@SuppressLint("ByteOrderMark")
class Firma : AppCompatActivity() {

    private var locationManager : LocationManager? = null
    private val LOCATION_REQUEST=1500

    companion object {

        val TAG = "PermissionDemo"
        private val REQUEST_PERMISSION = 2000
        private const val REQUEST_INTERNET = 200
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firma)

        val  sharedPreference =  getSharedPreferences(
            "mi_app_music",
            Context.MODE_PRIVATE
        )

        FlagLocalization.setOnClickListener {
            revisaPermisoFlag()
        }

        val usrNombre = sharedPreference.getString("usr_nombre","")
        txtNombreFirma.setText(usrNombre)

        revisaPermiso()

        PadFirma.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() { //Toast.makeText(SignActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            override fun onSigned() {
                mSaveButton.isEnabled = true
                mClearButton.isEnabled = true
            }

            override fun onClear() {
                mSaveButton.isEnabled = false
                mClearButton.isEnabled = false
            }
        })

        mSaveButton.setOnClickListener {
            val signatureBitmap: Bitmap = PadFirma.transparentSignatureBitmap
            if (addJpgSignatureToGallery(signatureBitmap)) { //Toast.makeText(SignActivity.this, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                PadFirma.clear()
            } else {
                Toast.makeText(
                    this,
                    "Unable to store the signature",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        mClearButton.setOnClickListener { PadFirma.clear() }

    }


    fun revisaPermiso(){
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_INTERNET
            )
            Log.i(TAG, "Pide permiso")
        }

    }
    fun addJpgSignatureToGallery(signature: Bitmap): Boolean {
        var result = false
        try {
            val path =
                Environment.getExternalStorageDirectory().toString() + "/checador"
            Log.d("Files", "Path: $path")
            val fileFirm = File(path)
            fileFirm.mkdirs()
            val photo =
                File(fileFirm, "Firma.png")
            Log.d("Files", "Path: $photo")
            saveBitmapToPNG(signature, photo)
            Log.e("Firma", "-->"+photo)
            Log.e("Firma", "-->"+fileFirm )
            Log.e("Firma", "-->"+saveBitmapToPNG(signature, photo))
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    @Throws(IOException::class)
    fun saveBitmapToPNG(bitmap: Bitmap, photo: File) {
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(photo)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                out?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun revisaPermisoFlag(){
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_REQUEST
            )
            Log.i(TAG, "Pide permiso")
        }else
            obtenLocacion()

    }
    fun obtenLocacion(){
        try {
            Toast.makeText(this, "Obteniendo...", Toast.LENGTH_SHORT).show()
            // Request location updates
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener);
        } catch(ex: SecurityException) {
            Log.d(TAG, "Security Exception, no location available");
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_REQUEST -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                obtenLocacion()
            }else{
                Toast.makeText(this, "Se requiere el permiso...", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            tvLoc.text="${location.longitude} : ${location.latitude}"
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

}
