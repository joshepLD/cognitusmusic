package com.example.cognitus.adapter



import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cognitus.R
import com.example.cognitus.activities.DetailProduct
import com.example.cognitus.activities.ListProductos
import com.example.cognitus.model.model.Productos
import kotlinx.android.synthetic.main.item_producto.view.*


class ProductoAdapter(
    private val context: ListProductos,
    private val productosList: List<Productos>) : RecyclerView.Adapter<ProductoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_producto, parent, false))
    }

    override fun getItemCount(): Int {
        return productosList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val productoModel=productosList.get(position)
        holder.tituloProducto?.text = productoModel.PTitulo
        holder.precioProducto?.text = productoModel.PPrecio
        val requestManager = Glide.with(context)
        val imageUri = productoModel.PImagen
        val requestBuilder = requestManager.load("http://35.155.161.8:8080/WSExample/${imageUri}")
        requestBuilder.into(holder.fotoProducto)
       holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailProduct::class.java)
            intent.putExtra("img_url", ""+productoModel.PImagen)
            intent.putExtra("title", ""+productoModel.PTitulo)
            intent.putExtra("precio", ""+productoModel.PPrecio)
            intent.putExtra("desc", ""+productoModel.PDesc)
            intent.putExtra("prodID", ""+productoModel.PId)

            context.startActivity(intent)
        }
        /*holder.ivVerMas.setOnClickListener {
            Toast.makeText(
                context,
                "Click en el icono",
                Toast.LENGTH_SHORT
            ).show()
        }*/
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tituloProducto = view.tvTituloProd
        val fotoProducto = view.ivFotoProd
        val precioProducto = view.tvPrecioProd
        //val ivVerMas = view.ivVerMas
    }
}
