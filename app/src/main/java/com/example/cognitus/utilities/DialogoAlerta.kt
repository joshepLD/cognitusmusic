package com.example.cognitus.utilities

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import com.example.cognitus.R
import kotlinx.android.synthetic.main.view_dialog.view.*

object DialogoAlerta {
    fun crearDialogo(context: Context, titulo: String, mensaje: String) {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.view_dialog, null)

        val dialogBuilder = AlertDialog.Builder(context).setView(mDialogView)
        mDialogView.tvMsg?.text=mensaje
        mDialogView.tvTitulo?.text=titulo

        val alert = dialogBuilder.create()

        alert.show()
        mDialogView.btnContinuar.setOnClickListener {
            alert.dismiss()
        }

    }
}