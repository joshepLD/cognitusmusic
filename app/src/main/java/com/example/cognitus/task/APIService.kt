package com.example.cognitus.task

import com.example.cognitus.model.response.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface APIService {

    @GET("DataWS?ApiCall=getLoginUsr")
    fun getCharacterByName(@Query ("email") correo:String, @Query ("nip") contraseña:String): Call<UsuarioResponse>

    //?ApiCall=setRegisterUsr Registro
    @Multipart
    @POST("DataWS")
    fun getCharacterByRegistrer(
        @Part partMap: List<MultipartBody.Part> ): Call<RegistroResponse>

    // Actualizar
    @Multipart
    @POST("DataWS")
    fun getCharacterByActualizar(
        @Part partMap: List<MultipartBody.Part> ): Call<ActualizarResponse>

    /*@GET("DataWS?ApiCall=updateUsr")
    fun getCharacterByActualizar(
        @Query ("usrid") idA:String,
        @Query ("email") correoA:String,
        @Query ("nip") contraA:String,
        @Query ("nombre") nombreA:String,
        @Query ("app") appA:String,
        @Query ("apm") apmA:String
    ): Call<ActualizarResponse>*/

    @GET("DataWS?ApiCall=recoveryPSw")
    fun getCharacterByRecuperar(@Query ("email") correoRec:String, @Query ("nombre") nombreRec:String): Call<RecuperarResponse>


    @GET("DataWS?ApiCall=getProductos")
    fun getCharacterByProductos(): Call<ProductosResponse>

    @GET("DataWS?ApiCall=setProdOrder")
    fun getOrden(
        @Query ("id_producto") idOrden:String
    ): Call<OrdenResponse>
}


//photoFile?.crearMultiparte