package com.example.cognitus.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.cognitus.DateTypeConverter
import com.example.cognitus.dao.TaskDao
import com.example.cognitus.entity.TaskEntity

@Database(entities = arrayOf(TaskEntity::class), version = 1)
@TypeConverters(DateTypeConverter::class)
abstract class TasksDatabase : RoomDatabase() {
    abstract fun taskDao(): TaskDao
}